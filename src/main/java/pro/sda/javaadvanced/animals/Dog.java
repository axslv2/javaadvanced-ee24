package pro.sda.javaadvanced.animals;

public class Dog extends Animal {
    private String name;
    // "M" "F"
    private String gender;
    private Integer weight;
    private String race;


    @Override
    public void setAge(Integer age) {
        if (age >= 20) {
            System.out.println("Too old for a dog, should be dead or not a dog.");
            return;
        }
        super.setAge(age);
    }

    //All args constructor
    public Dog(String name, Integer age, String gender, Integer weight, String race) {
        this.name = name;
        super.setAge(age);
        this.gender = gender;
        setWeight(weight);
        this.race = race;
        super.getDomain();
    }

    public Dog Dog(String gender, String race) {
       return new Dog("Sparky", 1, gender, 1, race);
    }



    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        if (weight <= 0) {
            System.out.println("Incorrect weight was specified. Please, use values greater than zero!");
            return;
        }
        this.weight = weight;
    }
}
