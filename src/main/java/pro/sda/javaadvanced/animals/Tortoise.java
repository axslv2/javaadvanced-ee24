package pro.sda.javaadvanced.animals;

public class Tortoise extends Animal {

    @Override
    public void setAge(Integer age) {
        if (age >= 300) {
            System.out.println("Too old for dog, should be dead.");
            return;
        }
        super.setAge(age);
    }
}
