package pro.sda.javaadvanced.garage;

//POJO
public class Car {
    private Color color;
    private Engine engine;

    //No arguments constructor, available by default, no need to specify it explicitly
    public Car() {
    }

    //All args constructor
    //Signature - method name + parameter count + parameter datatype - simultaneously
    public Car(Color color, Engine engine) {
        this.engine = engine;
        this.color = color;
    }

    public void setColor(Color c) {
        this.color = c;
    }

    public Color getColor() {
        return this.color;
    }

    public void setEngine(Engine e) {
        this.engine = e;
    }

    public Engine getEngine() {
        return this.engine;
    }
}
